#include <stdio.h>
#include <unistd.h>

#include "log.h"
#include "process.h"

int main(int argc, char *argv[]) {

	log_t *log = log_open("/var/log/hserver/error.log");
	if (!log) {
		fprintf(stderr, "can\'t open log file\n");
		return -1;
	}

	log_info(log, "\n");
	log_info(log, "hserver started");

	if (process_daemonize() == -1) {
		log_error(log, "can\'t daemonize process");
		return -1;
	}

	if (process_setup_signals() == -1) {
		log_error(log, "can\'t setup signals");
		return -1;
	}

	while (1) {
		sleep(1);

		if (process_is_exited)
			break;
	}

	log_info(log, "hserver stopped");

	log_close(log);

	return 0;
}
