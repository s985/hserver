all: hserver

hserver: hserver.o log.o process.o
	gcc -o $@ $^

%.o: %.c
	gcc -g -o $@ -c $^

clean:
	rm -f hserver

.PFONY: all clean
